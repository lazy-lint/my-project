# Lazy lint My Project


:warning: :warning This does not work as long as https://gitlab.com/gitlab-org/gitlab/-/issues/349020 is not resolved :warning: :warning:


Goal of this repository is to provide a simple and easy way to apply code
quality tools via GitLab CI to projects.
As a lot of developers do not even know which tools are available for their
code, this should show them, which tools they can use, and easily apply them.
The lazy developer might not want to adapt it at all, and will be fine with the
basic configuration.

We are utilizing the possibility to include external CI configuration and the
newly added key word `exists` to automatically apply linters.

It should be easy to configure, and easy to adapt, and most importantly it
should automatically find files which should be linted.

## Supported linters

- yamllint (YAMLLINT)
- markdownlint (MD_LINT)
- markdown-spellcheck (MD_SPELLCHECK)
- hadolint (HADOLINT)
- go-lint (GO_LINT)
- ... more to come

## Usage

```yaml
include:
  - project: lazy-lint/my-project
    ref: main # use tags later on for this
    files: Project.gitlab-ci.yml
```

## Configuration

There is a general "private" job called `.lint`. If you want to adapt the stage
the linters are run in, or some kind of rules. Please overwrite it and add your
configuration

Each linter can be configured with variables. The naming scheme is
`LLMP_<LINTER_NAME>_*`. Currently we support to possible configurations:

- DISABLE: which will disable the linter
- ARGS: which allows you to specify custom arguments
- FORCE: to force the linter to run, even without changes (eg. if the files are
    generated in a previous stage)

## shout outs

### pipeline-components.dev

[Pipeline Components](https://gitlab.com/pipeline-components)

They provide a great set of linters already, which we simply used in this proof
of concept.
Thank you for this great collection.
